const show = [
  {season: 1, episodes: 12},
  {season: 2, episodes: 16},
  {season: 3, episodes: 10},
  {season: 4, episodes: 20},
  {season: 5, episodes: 21},
  {season: 6, episodes: 20},
  {season: 7, episodes: 40},
  {season: 8, episodes: 72}
];

let container = document.querySelector('#htmlOutput');

document.getElementById('randomBtn').addEventListener('click', function() {

  const date = new Date().toLocaleTimeString();

  // Pick a season
  let randomSeason = Math.floor( Math.random() * show.length);
  console.log(` Random Season = ${randomSeason + 1}`);

  // Pick an episode
  let randomEpisode = Math.round( Math.random() * show[randomSeason].episodes);
  console.log(` Random Episode = ${randomEpisode + 1}`);

  let oldHTML = document.getElementById('htmlOutput').innerHTML;

  container.innerHTML = `<li>${date} | Season: <b>${randomSeason + 1}</b>, Episode <b>${randomEpisode + 1}</b>.</li>` + oldHTML;

});